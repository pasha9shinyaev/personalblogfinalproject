﻿using FinalBLL.Interfaces;
using FinalDAL;
using FinalDAL.Models;
using FinalDAL.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalBLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public Task<IList<string>> GetUserRoles(string username)
        {
            return _userRepository.GetUserRoles(username);
        }
        public async Task<IList<Role>> GetAllRoles()
        {
            return await _userRepository.GetAllRoles();
        }
        public async Task<bool> IsAdmin(string userName)
        {
            return await _userRepository.IsAdmin(userName);
        }
        public Task<IdentityResult> AddUserToRole(string userName, Role roleName)
        {
            return _userRepository.AddUserToRole(userName, roleName);
        }

        public Task<Role> CreateRole(string roleName)
        {
            return _userRepository.CreateRole(roleName);
        }

        public User SignIn(UserInfo userLogin)
        {
            return _userRepository.SignIn(userLogin);
        }

        public Task<bool> SignUp(User signUp)
        {
            return _userRepository.SignUp(signUp);
        }

        public User GetUserByName(string userName)
        {
            return _userRepository.GetUserByName(userName);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _userRepository.GetAll();
        }

        public  IdentityResult Update(User user)
        {
            return _userRepository.Update(user);
        }

        public async Task<bool> Delete(string userName)
        {
            return await _userRepository.Delete(userName);
        }
    }
}
