﻿using FinalDAL.Models;
using FinalDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalBLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }
        public async Task<Comment> CreateComment(Comment item)
        {
            return await _commentRepository.CreateComment(item);
        }
        public async Task<IEnumerable<Comment>> GetAllComments()
        {
            return await _commentRepository.GetAllComments();
        }

        public async Task<IEnumerable<Comment>> GetAllCommentsForArticle(int id)
        {
            return await _commentRepository.GetAllCommentsForArticle(id);
        }

        public async Task<Comment> GetCommentById(int id)
        {
            return await _commentRepository.GetCommentById(id);
        }

        public async Task<Comment> Update(Comment item)
        {
            return await _commentRepository.Update(item);
        }
        public async Task<bool> Delete(int id)
        {
            return await _commentRepository.Delete(id);
        }
    }
}
