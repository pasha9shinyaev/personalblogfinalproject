﻿using FinalDAL.Models;
using FinalDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalBLL.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IArticleRepository _articleRepository;
        public ArticleService(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }
        public Task<Article> CreateArticle(Article item)
        {
            return _articleRepository.CreateArticle(item);
        }

        public Task<bool> Delete(int id)
        {
            return _articleRepository.Delete(id);
        }

        public Task<IEnumerable<Article>> GetAll()
        {
            return _articleRepository.GetAll();
        }


        public Task<Article> GetById(int id)
        {
            return _articleRepository.GetById(id);
        }

        public async Task<IEnumerable<Article>> GetAllArticlesForBlog(int id)
        {
            return await _articleRepository.GetAllArticlesForBlog(id);
        }
        public Task<Article> Update(Article item)
        {
            return _articleRepository.Update(item);
        }
    }
}
