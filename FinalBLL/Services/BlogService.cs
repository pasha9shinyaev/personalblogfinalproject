﻿using FinalDAL.Models;
using FinalDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalBLL.Services
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;
        public BlogService(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }
        public Task<Blog> CreateBlog(Blog item)
        {
            return _blogRepository.CreateBlog(item);
        }

        public Task<bool> Delete(int id)
        {
            return _blogRepository.Delete(id);
        }

        public Task<IEnumerable<Blog>> GetAll()
        {
            return _blogRepository.GetAll();
        }

        public Task<Blog> GetById(int id)
        {
            return _blogRepository.GetById(id);
        }

        public Task<Blog> Update(Blog item)
        {
            return _blogRepository.Update(item);
        }
    }
}
