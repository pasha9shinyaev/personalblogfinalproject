﻿using FinalDAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalBLL.Services
{
    public interface IArticleService
    {
        Task<Article> CreateArticle(Article item);
        Task<IEnumerable<Article>> GetAll();
        Task<Article> GetById(int id);
        Task<IEnumerable<Article>> GetAllArticlesForBlog(int id);
        Task<Article> Update(Article item);
        Task<bool> Delete(int id);

    }
}