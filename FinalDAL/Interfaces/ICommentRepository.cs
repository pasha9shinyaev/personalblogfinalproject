﻿using FinalDAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public interface ICommentRepository
    {
        Task<Comment> CreateComment(Comment item);
        Task<IEnumerable<Comment>> GetAllComments();
        Task<Comment> GetCommentById(int id);
        Task<IEnumerable<Comment>> GetAllCommentsForArticle(int id);
        Task<Comment> Update(Comment item);
        Task<bool> Delete(int id);
    }
}