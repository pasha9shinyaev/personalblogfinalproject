﻿using FinalDAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public interface IUserRepository
    {
        
        Task<bool> SignUp(User signUp);
        User SignIn(UserInfo userLogin);
        Task<Role> CreateRole(string roleName);
        Task<IList<string>> GetUserRoles(string username);
        Task<IList<Role>> GetAllRoles();
        Task<bool> IsAdmin(string userName);
        Task<IdentityResult> AddUserToRole(string userName, Role roleName);
        User GetUserByName(string userName);
        Task<IEnumerable<User>> GetAll();
        IdentityResult Update(User user);
        Task<bool> Delete(string userName);
    }
}