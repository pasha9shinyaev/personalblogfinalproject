﻿using FinalDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public interface IBlogRepository
    {
        Task<Blog> CreateBlog(Blog item);
        Task<IEnumerable<Blog>> GetAll();
        Task<Blog> GetById(int id);
        Task<Blog> Update(Blog item);
        Task<bool> Delete(int id);

    }
}
