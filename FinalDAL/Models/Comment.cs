﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalDAL.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime CreationTime { get; set; }
        public int ArticleId { get; set; }
    }
}
