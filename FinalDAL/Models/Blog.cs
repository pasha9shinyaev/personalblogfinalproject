﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace FinalDAL.Models
{
    public class Blog
    {
        [SwaggerSchema(ReadOnly = true)]
        public int BlogId { get; set; }
        [Required]
        [MinLength(5)]
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public ICollection<Article> Articles { get; set; }
        public string UserName { get; set; }

    }
}
