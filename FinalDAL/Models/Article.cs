﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace FinalDAL.Models
{
    public class Article
    {
        [SwaggerSchema(ReadOnly = true)]
        public int ArticleId { get; set; }
        [Required]
        [MinLength(5)]
        public string Name { get; set; }
        [Required]
        public string Text { get; set; }
        public DateTime CreationTime { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public int BlogId { get; set; }
    }
}
