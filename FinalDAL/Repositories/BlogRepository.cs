﻿using FinalDAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public class BlogRepository : IBlogRepository
    {
        private readonly MyDbContext _context;
        public BlogRepository(MyDbContext context)
        {
            _context = context;
        }
        public async Task<Blog> CreateBlog(Blog blog)
        {
            blog.CreationTime = DateTime.Now;
            await _context.Blogs.AddAsync(blog);
            await _context.SaveChangesAsync();
            return blog;
        }

        public async Task<bool> Delete(int id)
        {
            var item = _context.Blogs.FirstOrDefault(x => x.BlogId == id);
            var result = item != null;
            if (result)
            {
                _context.Blogs.Remove(item);
                await _context.SaveChangesAsync();
            }
            return result;
                        
        }

        public async Task<IEnumerable<Blog>> GetAll()
        {
            return await _context.Blogs.ToListAsync();
        }

        public async Task<Blog> GetById(int id)
        {
            return await _context.Blogs.FindAsync(id);
        }

        public async Task<Blog> Update(Blog blog)
        {
            if (_context.Blogs.Any(x => x.BlogId == blog.BlogId))
            {
                _context.Blogs.Update(blog);
                await _context.SaveChangesAsync();
                return blog;
            }
            else
            {
                return null;
            }
        }
    }
}
