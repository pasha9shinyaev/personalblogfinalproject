﻿using FinalDAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly MyDbContext _context;
        public UserRepository(UserManager<User> userManager, RoleManager<Role> roleManager, MyDbContext context)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        public async Task<bool> SignUp(User signUp)
        {
            var user = new User { UserName = signUp.UserName, Password = signUp.Password };
            var userCreateResult = await _userManager.CreateAsync(user);
            if (userCreateResult.Succeeded)
            {
                return true;
            }
            return false;
        }
        public User SignIn(UserInfo userLogin)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userLogin.Username);
            if (user is null)
            {
                return null;
            }
            var userSigninResult = user.Password == userLogin.Password;
            if (userSigninResult)
            {
                return user;
            }
            return null;
        }
        public async Task<Role> CreateRole(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentNullException();
            }
            var newRole = new Role
            {
                Name = roleName
            };
            var roleResult = await _roleManager.CreateAsync(newRole);
            if (roleResult.Succeeded)
            {
                return newRole;
            }
            return null;
        }
        public async Task<IList<Role>> GetAllRoles()
        {
            return await _roleManager.Roles.ToListAsync();
        }
        public async Task<IList<string>> GetUserRoles(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            return await _userManager.GetRolesAsync(user);
        }
        public async Task<bool> IsAdmin(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var result = _userManager.GetRolesAsync(user).Result.Contains("admin");
            return result;
        }
        public User GetUserByName(string userName)
        {
            return _context.Users.FirstOrDefault(x => x.UserName == userName);
        }
        public async Task<IEnumerable<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }
        public async Task<IdentityResult> AddUserToRole(string userName, Role roleName)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userName);
            if (_userManager.GetRolesAsync(user).Result.Any())
            {
                return null;
            }
            var result = await _userManager.AddToRoleAsync(user, roleName.Name);
            if (result.Succeeded)
            {
                return result;
            }
            return result;
        }
        public IdentityResult Update(User user)
        {
            User item = _userManager.FindByNameAsync(user.UserName).Result;
            if (item != null)
            {
                item.Password = user.Password;
                try { 
                IdentityResult result = _userManager.UpdateAsync(item).Result;
                    return result;
                }
                catch
                {
                    throw new Exception();
                }
            }
            return null;
        }
        public async Task<bool> Delete(string userName)
        {
            var item = _context.Users.FirstOrDefault(x => x.UserName == userName);
            var result = item != null;
            if (result)
            {
                _context.Users.Remove(item);
                await _context.SaveChangesAsync();
            }
            return result;
        }
    }
}
