﻿using FinalDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly MyDbContext _context;
        public ArticleRepository(MyDbContext context)
        {
            _context = context;
        }
        public async Task<Article> CreateArticle(Article item)
        {
            item.CreationTime = DateTime.Now;
            await _context.Articles.AddAsync(item);
            await _context.SaveChangesAsync();
            /*var blog = await _context.Blogs.FindAsync(item.BlogId);
            //blog.Articles.Add(item);
            item.Blog = blog; 
            if (_context.Blogs.Any(x => x.BlogId == item.BlogId))
            {
                _context.Blogs.Update(item.Blog);
            }*/
            //_context.Blogs.Attach(blog);
            //_context.Entry(blog).State = EntityState.Modified;
            //await _context.SaveChangesAsync();
            return item;
        }
        public async Task<IEnumerable<Article>> GetAll()
        {
            return await _context.Articles.ToListAsync();
        }
        public async Task<IEnumerable<Article>> GetAllArticlesForBlog(int id)
        {
            var result = await _context.Blogs.FindAsync(id);
            foreach (var article in _context.Articles)
            {
                if(article.BlogId == id)
                {
                    result.Articles.Add(article);
                }
            }
            return result.Articles;
        }
        public async Task<Article> GetById(int id)
        {
            return await _context.Articles.FindAsync(id);
        }

        public async Task<Article> Update(Article item)
        {
            if (_context.Articles.Any(x => x.ArticleId == item.ArticleId))
            {
                _context.Articles.Update(item);
                await _context.SaveChangesAsync();
                return item;
            }
            else
            {
                return null;
            }
        }
        public async Task<bool> Delete(int id)
        {
            var item = _context.Articles.FirstOrDefault(x => x.ArticleId == id);
            var result = item != null;
            if (result)
            {
                _context.Articles.Remove(item);
                await _context.SaveChangesAsync();
            }
            return result;
        }
    }
}
