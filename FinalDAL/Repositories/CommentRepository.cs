﻿using FinalDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalDAL.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly MyDbContext _context;
        public CommentRepository(MyDbContext context)
        {
            _context = context;
        }
        public async Task<Comment> CreateComment(Comment item)
        {
            item.CreationTime = DateTime.Now;
            await _context.Comments.AddAsync(item);
            await _context.SaveChangesAsync();
            return item;
        }
        public async Task<IEnumerable<Comment>> GetAllComments()
        {
            return await _context.Comments.ToListAsync();
        }

        public async Task<IEnumerable<Comment>> GetAllCommentsForArticle(int id)
        {
            var result = await _context.Articles.FindAsync(id);
            foreach (var comment in _context.Comments)
            {
                if ( comment.ArticleId == id)
                {
                    result.Comments.Add(comment);
                }
            }
            
            return result.Comments;
        }

        public async Task<Comment> GetCommentById(int id)
        {
            return await _context.Comments.FindAsync(id);
        }
        public async Task<Comment> Update(Comment item)
        {
            if (_context.Comments.Any(x => x.CommentId == item.CommentId))
            {
                _context.Comments.Update(item);
                await _context.SaveChangesAsync();
                return item;
            }
            else
            {
                return null;
            }
        }
        public async Task<bool> Delete(int id)
        {
            var item = _context.Comments.FirstOrDefault(x => x.CommentId == id);
            var result = item != null;
            if (result)
            {
                _context.Comments.Remove(item);
                await _context.SaveChangesAsync();
            }
            return result;
        }
    }
}
